import torch
import pandas as pd
import numpy as np
import spacy
import os

class Reader(object):
    def __init__(self, root, label, path=None, field='feature', target_field='target'):
        self.root = root
        self.path = path
        self.label = label
        self.field = field
        self.target_field = target_field
        
    def get_df(self):
        text = self.read_txt(self.root, self.path)
        labels = [self.label] * len(text)
        return pd.DataFrame({self.field: text, self.target_field: labels})
        
    def read_txt(self, path, file=None):
        if file == None:
            files = os.listdir(path)
            files = [f for f in files if '.txt' in f]
            strings = [open(os.path.join(path, f)).read() for f in files]
        else:
            strings = open(os.path.join(path, file)).read()
        return strings

class TextProcessor(object):
    def __init__(self, reader):
        self.reader = reader
        self.nlp = spacy.load('en')
        
    def to_tensor(self):
        df = self.reader.get_df()
        data = df[self.reader.field]
        labels = df[self.reader.target_field]
        data_tensor = torch.cat([self.doc_to_tensor(doc) for doc in data], 1)
        label_tensor = torch.cat([self.label_to_tensor(label) for label in labels])
        return data_tensor, label_tensor
        
    def doc_to_tensor(self, doc):
        doc = self.nlp(doc)
        vectors = [word.vector.reshape(1, 1, -1) for word in doc]
        vectors = np.concatenate(vectors)
        tensor = torch.from_numpy(vectors)
        return tensor
    
    def label_to_tensor(self, label):
        if label == 'pos':
            tensor = torch.Tensor([0, 1])
        else:
            tensor = torch.Tensor([1, 0])
        return tensor.view(1, -1)
