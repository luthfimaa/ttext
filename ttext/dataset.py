from torch.utils import data

class TextDataset(data.Dataset):
    def __init__(self, reader):
        self.df = reader.get_df() 
    
    def __len__(self):
        return self.df.shape[0]
    
    def __getitem__(self, index):
        return self.df.iloc[index][self.field], self.df.iloc[index][self.target]
